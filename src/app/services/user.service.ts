import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  SERVER_URL = "http://localhost:8080/api";

  constructor(private httpClient: HttpClient) {
  }

  public listUsers() {
    return this.httpClient.get(`${this.SERVER_URL}/users`);
  }

  public insertUser(user: any) {
    return this.httpClient.post(`${this.SERVER_URL}/user`, user);
  }

  public deleteUser(user: any) {
    return this.httpClient.delete(`${this.SERVER_URL}/user`, {
      headers: new HttpHeaders(),
      body: user
    });
  }

  public updateUser(user: any) {
    return this.httpClient.put(`${this.SERVER_URL}/user`, user);
  }
}
