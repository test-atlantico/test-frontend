import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Input() user = {
    name: "",
    username: "",
    password: "",
    createdDate: null,
    updatedDate: null,
    email: "",
    admin: false
  };

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
  }

  executeInsert(user: any) {
    let msg = "";
    if (!user.name) {
      msg += "Name is missing!\n";
    }

    if (!user.username) {
      msg += "Username is missing!\n";
    }

    if (!user.password) {
      msg += "Password is missing!\n";
    }

    if (!user.createdDate) {
      msg += "Created Date is missing!\n";
    }

    if (!user.email) {
      msg += "E-Mail is missing!";
    }

    if (msg) {
      alert(msg);
      return;
    }

    if (!confirm("Sure about this?")) {
      return;
    }

    this.userService.insertUser(user).subscribe(
      () => {
        alert("User inserted successfully!");
      },
      (error) => {
        console.log("Error: " + JSON.stringify(error))
      }
    );
  }
}