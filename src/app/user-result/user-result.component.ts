import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-result',
  templateUrl: './user-result.component.html',
  styleUrls: ['./user-result.component.css']
})
export class UserResultComponent implements OnInit {
  @Input() userList = new Array();
  @Output() callParent:EventEmitter<any> = new EventEmitter<any>();

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
  }

  executeDelete(user: any) {
    if (!confirm("Sure about this?"))
      return;

    this.userService.deleteUser(user).subscribe(
      () => {
        alert("User deleted successfully!");
      },
      (error) => {
        console.log("Error: " + JSON.stringify(error))
      }
    );
  }

  prepareUpdate(user: any) {
    this.callParent.emit(user);
  }
}
