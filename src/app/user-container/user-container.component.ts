import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-container',
  templateUrl: './user-container.component.html',
  styleUrls: ['./user-container.component.css']
})
export class UserContainerComponent implements OnInit {
  myUser: any;
  userList: any;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.listUsers().subscribe(
      (data) => {
        this.userList = data;
        console.log(JSON.stringify(data));
        console.table(data);
      },
      (error) => {
        console.log("Error: " + error)
      }
    );
  }

  parent(user: any) {
    this.myUser = user;
  }
}